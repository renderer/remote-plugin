var _ = require('lodash');

module.exports = {
  name: "Test remote plugin",
  description: "do nothing",
  requirements: ['model.User'],
  callback: function(User, register) {
    User.findAll().then(function(users) {
      console.log(users);
    });
  }
};